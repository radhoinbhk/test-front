import React from "react";
import PaperComponent from "components/paperComponent";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import gameReducer from "redux/game/gameSlice";
import { render, screen, fireEvent } from "@testing-library/react";

export const store = configureStore({
  reducer: {
    game: gameReducer,
  },
});

interface propsPaper {
  flippedTemp?: boolean;
  flippedFailed?: boolean;
  flippedSucc?: boolean;
  image: string;
  onClick(): void;
}

function Paper({
  flippedTemp,
  flippedSucc,
  flippedFailed,
  image,
  onClick,
}: propsPaper) {
  return render(
    <Provider store={store}>
      <PaperComponent
        flippedTemp={flippedTemp}
        flippedSucc={flippedSucc}
        flippedFailed={flippedFailed}
        onClick={onClick}
        image={image}
      />
    </Provider>
  );
}

describe("paper component tests", () => {
  it("Render correctly initial paper with out img", () => {
    var paperIsClicked = false;
    const { getAllByTestId } = Paper({
      flippedTemp: false,
      flippedSucc: false,
      flippedFailed: false,
      image: "jslogo-1",
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithoutImg = getAllByTestId("paper-withoutImg")[0];
    expect(paperWithoutImg).toHaveClass("paper");
    expect(paperWithoutImg).not.toHaveClass("flippedTemp");
    expect(paperWithoutImg).not.toHaveClass("flippedSucc");
    expect(paperWithoutImg).not.toHaveClass("flippedFailed");
    expect(paperWithoutImg).not.toHaveClass("disabled");
    expect(paperWithoutImg.childNodes.length).toEqual(0);
  });

  it("Render correctly initial paper with img", () => {
    var paperIsClicked = false;
    const { getAllByTestId } = Paper({
      flippedTemp: false,
      flippedSucc: false,
      flippedFailed: false,
      image: "jslogo-1",
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithImg = getAllByTestId("paper-withImg")[0];

    expect(paperWithImg).toHaveClass("paper");
    expect(paperWithImg).not.toHaveClass("flippedTemp");
    expect(paperWithImg).not.toHaveClass("flippedSucc");
    expect(paperWithImg).not.toHaveClass("flippedFailed");
    expect(paperWithImg).not.toHaveClass("disabled");
    expect(paperWithImg.childNodes.length).toEqual(0);
  });

  it("Paper with out img Should be render correctly whene flippedTemp props true", () => {
    var paperIsClicked = false;
    const { getAllByTestId } = Paper({
      flippedTemp: true,
      flippedSucc: false,
      flippedFailed: false,
      image: "jslogo-1",
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithoutImg = getAllByTestId("paper-withoutImg")[0];
    expect(paperWithoutImg).toHaveClass("paper");
    expect(paperWithoutImg).toHaveClass("flippedTemp");
    expect(paperWithoutImg).not.toHaveClass("flippedSucc");
    expect(paperWithoutImg).not.toHaveClass("flippedFailed");
    expect(paperWithoutImg).toHaveClass("disabled");
    expect(paperWithoutImg.childNodes.length).toEqual(0);
  });

  it("Paper with out img Should be render correctly whene flippedSucc props true", () => {
    var paperIsClicked = false;
    const { getAllByTestId } = Paper({
      flippedTemp: false,
      flippedSucc: true,
      flippedFailed: false,
      image: "jslogo-1",
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithoutImg = getAllByTestId("paper-withoutImg")[0];
    expect(paperWithoutImg).toHaveClass("paper");
    expect(paperWithoutImg).not.toHaveClass("flippedTemp");
    expect(paperWithoutImg).toHaveClass("flippedSucc");
    expect(paperWithoutImg).not.toHaveClass("flippedFailed");
    expect(paperWithoutImg).toHaveClass("disabled");
    expect(paperWithoutImg.childNodes.length).toEqual(0);
  });

  it("Paper with out img Should be render correctly whene flippedFailed props true", () => {
    var paperIsClicked = false;
    const { getAllByTestId } = Paper({
      flippedTemp: false,
      flippedSucc: false,
      flippedFailed: true,
      image: "jslogo-1",
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithoutImg = getAllByTestId("paper-withoutImg")[0];
    expect(paperWithoutImg).toHaveClass("paper");
    expect(paperWithoutImg).not.toHaveClass("flippedTemp");
    expect(paperWithoutImg).not.toHaveClass("flippedSucc");
    expect(paperWithoutImg).toHaveClass("flippedFailed");
    expect(paperWithoutImg).not.toHaveClass("disabled");
    expect(paperWithoutImg.childNodes.length).toEqual(0);
  });

  it("Paper with img Should be render correctly whene flippedTemp props true", () => {
    var paperIsClicked = false;
    const image = "jslogo-1";
    const { getAllByTestId } = Paper({
      flippedTemp: true,
      flippedSucc: false,
      flippedFailed: false,
      image: image,
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithImg = getAllByTestId("paper-withImg")[0];
    const childPaper = paperWithImg.childNodes;
    const img = screen.getAllByRole("img");
    const displayedImage = img[0] as HTMLImageElement;

    expect(paperWithImg).toHaveClass("paper");
    expect(paperWithImg).toHaveClass("flippedTemp");
    expect(paperWithImg).not.toHaveClass("flippedSucc");
    expect(paperWithImg).not.toHaveClass("flippedFailed");
    expect(paperWithImg).toHaveClass("disabled");
    expect(childPaper.length).toEqual(1);
    expect(img.length).toEqual(1);
    expect(img[0]).toEqual(childPaper[0]);
    expect(displayedImage.src).toContain(image);
  });

  it("Paper with img Should be render correctly whene flippedSucc props true", () => {
    var paperIsClicked = false;
    const image = "jslogo-1";
    const { getAllByTestId } = Paper({
      flippedTemp: false,
      flippedSucc: true,
      flippedFailed: false,
      image: "jslogo-1",
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithImg = getAllByTestId("paper-withImg")[0];
    const childPaper = paperWithImg.childNodes;
    const img = screen.getAllByRole("img");
    const displayedImage = img[0] as HTMLImageElement;

    expect(paperWithImg).toHaveClass("paper");
    expect(paperWithImg).not.toHaveClass("flippedTemp");
    expect(paperWithImg).toHaveClass("flippedSucc");
    expect(paperWithImg).not.toHaveClass("flippedFailed");
    expect(paperWithImg).toHaveClass("disabled");
    expect(childPaper.length).toEqual(1);
    expect(img.length).toEqual(1);
    expect(img[0]).toEqual(childPaper[0]);
    expect(displayedImage.src).toContain(image);
  });

  it("Paper with img Should be render correctly whene flippedFailed props true", () => {
    var paperIsClicked = false;
    const { getAllByTestId } = Paper({
      flippedTemp: false,
      flippedSucc: false,
      flippedFailed: true,
      image: "jslogo-1",
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithImg = getAllByTestId("paper-withImg")[0];
    expect(paperWithImg).toHaveClass("paper");
    expect(paperWithImg).not.toHaveClass("flippedTemp");
    expect(paperWithImg).not.toHaveClass("flippedSucc");
    expect(paperWithImg).toHaveClass("flippedFailed");
    expect(paperWithImg).not.toHaveClass("disabled");
    expect(paperWithImg.childNodes.length).toEqual(0);
  });

  it("Paper with img Should be render correctly whene is clicked and not disabled", () => {
    var paperIsClicked = false;
    const { getAllByTestId } = Paper({
      flippedTemp: false,
      flippedSucc: false,
      flippedFailed: false,
      image: "jslogo-1",
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithImg = getAllByTestId("paper-withImg")[0];
    fireEvent.click(paperWithImg);
    expect(paperIsClicked).toEqual(true);
  });

  it("Paper with img Should be render correctly whene is clicked and disabled", () => {
    var paperIsClicked = false;
    const { getAllByTestId } = Paper({
      flippedTemp: true,
      flippedSucc: false,
      flippedFailed: false,
      image: "jslogo-1",
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithImg = getAllByTestId("paper-withImg")[0];
    fireEvent.click(paperWithImg);
    expect(paperIsClicked).toEqual(false);
  });

  it("Paper without img Should be render correctly whene is clicked and not disabled", () => {
    var paperIsClicked = false;
    const { getAllByTestId } = Paper({
      flippedTemp: false,
      flippedSucc: false,
      flippedFailed: false,
      image: "jslogo-1",
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithImg = getAllByTestId("paper-withoutImg")[0];
    fireEvent.click(paperWithImg);
    expect(paperIsClicked).toEqual(true);
  });

  it("Paper without img Should be render correctly whene is clicked and disabled", () => {
    var paperIsClicked = false;
    const { getAllByTestId } = Paper({
      flippedTemp: true,
      flippedSucc: false,
      flippedFailed: false,
      image: "jslogo-1",
      onClick: () => {
        paperIsClicked = true;
      },
    });
    const paperWithImg = getAllByTestId("paper-withoutImg")[0];
    fireEvent.click(paperWithImg);
    expect(paperIsClicked).toEqual(false);
  });
});
