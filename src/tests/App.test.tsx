import React from "react";
import { render, screen } from "@testing-library/react";
import { Provider } from 'react-redux'
import { configureStore } from "@reduxjs/toolkit";
import gameReducer from "redux/game/gameSlice";

export const store = configureStore({
    reducer: {
      game: gameReducer,
    },
  });
import App from "../App";

describe("App", () => {
  it("renders correctly", () => {
    render(<Provider store={store}><App /></Provider>);
  });
});
