import React from "react";
import ReactDOM from "react-dom";
import MemoryGame from "pages/memoryGame";
import { creatRandomArray, imgIsCompatible } from "pages/memoryGame";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import gameReducer from "redux/game/gameSlice";

export const store = configureStore({
  reducer: {
    game: gameReducer,
  },
});

describe("MemoryGame component tests", () => {
  let container: HTMLDivElement;

  beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
    ReactDOM.render(
      <Provider store={store}>
        <MemoryGame />
      </Provider>,
      container
    );
  });

  afterEach(() => {
    document.body.removeChild(container);
    container.remove();
  });

  it("Render correctly initial document", () => {
    const containerGame = container.getElementsByClassName("container-game");
    const childContainerGame = containerGame[0].childElementCount;
    const containerPaper = container.getElementsByClassName("container-paper");

    expect(containerGame).toHaveLength(1);
    expect(childContainerGame).toEqual(2);
    expect(containerPaper).toHaveLength(1);
  });
});

describe("Testing creatRandomArray function", () => {
  const randomArray = creatRandomArray();
  it("should return array", () => {
    expect(Array.isArray(randomArray)).toBe(true);
  });

  it("length of array should be equal 16", () => {
    expect(randomArray).toHaveLength(16);
  });

  it("should contain all the number", () => {
    expect(randomArray).toContainEqual(0);
    expect(randomArray).toContainEqual(1);
    expect(randomArray).toContainEqual(2);
    expect(randomArray).toContainEqual(3);
    expect(randomArray).toContainEqual(4);
    expect(randomArray).toContainEqual(5);
    expect(randomArray).toContainEqual(6);
    expect(randomArray).toContainEqual(7);
    expect(randomArray).toContainEqual(8);
    expect(randomArray).toContainEqual(9);
    expect(randomArray).toContainEqual(10);
    expect(randomArray).toContainEqual(11);
    expect(randomArray).toContainEqual(12);
    expect(randomArray).toContainEqual(13);
    expect(randomArray).toContainEqual(14);
    expect(randomArray).toContainEqual(15);
  });

  it("should be return a random value", () => {
    const arrayOfNumber = [
      0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
    ];
    expect(randomArray).not.toBe(arrayOfNumber);
  });

  it("should be a random return", () => {
    const newRandomArray = creatRandomArray();
    expect(randomArray).not.toBe(newRandomArray);
  });
});

describe("Testing imgIsCompatible function", () => {
  it("should be return true is Compatible", () => {
    const newRandomArray = imgIsCompatible('jslogo-1','jslogo-2');
    expect(newRandomArray).toBe(true);
  });

  it("should be return false is not Compatible", () => {
    const newRandomArray = imgIsCompatible('jslogo-1','nextjs-1');
    expect(newRandomArray).toBe(false);
  });
});

// 50.72 
