import JavascriptLogo from "./javascript.svg";
import NextJSLogo from "./next-js.svg";
import NodeJSLogo from "./nodejs.svg";
import NpmLogo from "./npm.svg";
import ReactNativeFirebaseLogo from "./react-native-firebase.svg";
import ReactLogo from "./react.svg";
import ReduxLogo from "./redux.svg";
import TypescriptLogo from "./typescript.svg";
import YouWinImg from "./you-win.png";

export {
  JavascriptLogo,
  NextJSLogo,
  NodeJSLogo,
  NpmLogo,
  ReactNativeFirebaseLogo,
  ReactLogo,
  ReduxLogo,
  TypescriptLogo,
  YouWinImg
};