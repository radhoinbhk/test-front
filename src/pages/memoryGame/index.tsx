import React, { useEffect, useState } from "react";
import { data } from "services/data";
import PaperComponent from "components/paperComponent";
import Timeline from "components/timeline";
import "styles/memoryGame.sass";
import {
  addflippedCards,
  addSupposedCards,
  selectSupposedCards,
  selectFlippedCards,
  resetSupposedCards,
  selectSupposedCardsIsFailed,
  setSupposedCardsIsFailed,
  resetGame,
} from "redux/game/gameSlice";
import { useAppDispatch, useAppSelector } from "redux/hooks";
import DialogComponent from "components/dialogComponent";

export const creatRandomArray = (): number[] => {
  for (var arr = [], i = 0; i < 16; ++i) arr[i] = i;

  var tmp,
    cur,
    tp = arr.length;
  if (tp)
    while (--tp) {
      cur = Math.floor(Math.random() * (tp + 1));
      tmp = arr[cur];
      arr[cur] = arr[tp];
      arr[tp] = tmp;
    }
  return arr;
};

export const imgIsCompatible = (img1: string, img2: string) => {
  const nameImg1 = img1.substring(0, img1.length - 1);
  const nameImg2 = img2.substring(0, img2.length - 1);
  return nameImg1 === nameImg2;
};

const MemoryGame = () => {
  const randomArrayType: number[] = [];
  const [randomArray, setRandomArray] = useState(randomArrayType);
  const [progress, setProgress] = useState(0);
  const [resultOfGame, setResultOfGame] = useState("");
  const [gameIsOver, setGameIsOver] = useState(false);
  const dispatch = useAppDispatch();
  const supposedCards = useAppSelector(selectSupposedCards);
  const flippedCards = useAppSelector(selectFlippedCards);
  const supposedCardsIsFailed = useAppSelector(selectSupposedCardsIsFailed);

  useEffect(() => {
    setRandomArray(creatRandomArray());
  }, []);

  useEffect(() => {
    const timer = setInterval(() => {
      if (!gameIsOver) {
        setProgress((oldProgress) => {
          if (oldProgress === 100) {
            return 0;
          }
          return Math.min(oldProgress + 100 / 120, 100);
        });
      }
    }, 500);

    return () => {
      clearInterval(timer);
    };
  }, [gameIsOver]);

  useEffect(() => {
    const gameFinished = flippedCards.length === data.length;
    const timeOut = progress === 100;
    if (timeOut || gameFinished) {
      setGameIsOver(true);
      setResultOfGame(gameFinished ? "YOU WIN!" : "YOU LOSE");
    }
  }, [progress, flippedCards.length]);

  useEffect(() => {
    if (supposedCards.length === 2) {
      if (!imgIsCompatible(supposedCards[0], supposedCards[1])) {
        setTimeout(() => supposedCardsIsFalse(), 400);
      } else {
        setTimeout(() => {
          dispatch(addflippedCards(supposedCards));
          dispatch(resetSupposedCards());
        }, 400);
      }
    }
  }, [supposedCards]);

  const supposedCardsIsFalse = async () => {
    await dispatch(setSupposedCardsIsFailed(true));
    setTimeout(() => {
      dispatch(resetSupposedCards());
      dispatch(setSupposedCardsIsFailed(false));
    }, 400);
  };

  const onPaperClick = (id: string) => () => {
    if (!supposedCards.includes(id)) {
      dispatch(addSupposedCards(id));
    }
  };

  const newGame = () => {
    dispatch(resetGame());
    setRandomArray(creatRandomArray());
    setGameIsOver(false);
    setProgress(0);
  };

  return (
    <div className="container-game">
      <div className="container-paper">
        {randomArray.map((val) => {
          const { logo, id } = data[val];
          return (
            <PaperComponent
              flippedTemp={supposedCards.includes(id)}
              flippedSucc={flippedCards.includes(id)}
              flippedFailed={
                supposedCardsIsFailed && supposedCards.includes(id)
              }
              image={logo}
              onClick={onPaperClick(id)}
              key={id}
            />
          );
        })}
      </div>
      <Timeline progress={progress} />
      <DialogComponent
        gameOver={gameIsOver}
        resultOfGame={resultOfGame}
        setNewGame={newGame}
      />
    </div>
  );
};

export default MemoryGame;
