import React from 'react';
import './App.sass';
import MemoryGame from 'pages/memoryGame'

function App() {
  return (
    <div className="App">
      <MemoryGame />
    </div>
  );
}

export default App;
