import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../store";

type GameStatus = "finished" | "loading" | "waiting";

export interface GameState {
  flippedCards: string[];
  supposedCards: string[];
  supposedCardsIsFailed: boolean;
}

const initialState: GameState = {
  flippedCards: [],
  supposedCards: [],
  supposedCardsIsFailed: false,
};

export const gameSlice = createSlice({
  name: "game",
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    addflippedCards: (state, action: PayloadAction<string[]>) => {
      state.flippedCards.push(...action.payload);
    },
    setSupposedCardsIsFailed: (state, action: PayloadAction<boolean>) => {
      state.supposedCardsIsFailed = action.payload;
    },
    addSupposedCards: (state, action: PayloadAction<string>) => {
      state.supposedCards.push(action.payload);
    },
    resetSupposedCards: (state) => {
      state.supposedCards = [];
    },
    resetGame: () => initialState,
  },
});

export const {
  addflippedCards,
  addSupposedCards,
  resetSupposedCards,
  resetGame,
  setSupposedCardsIsFailed,
} = gameSlice.actions;

export const selectFlippedCards = (state: RootState) => state.game.flippedCards;
export const selectSupposedCards = (state: RootState) =>
  state.game.supposedCards;
export const selectSupposedCardsIsFailed = (state: RootState) =>
  state.game.supposedCardsIsFailed;

export default gameSlice.reducer;
