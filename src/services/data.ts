import {
    JavascriptLogo,
    NextJSLogo,
    NodeJSLogo,
    NpmLogo,
    ReactNativeFirebaseLogo,
    ReactLogo,
    ReduxLogo,
    TypescriptLogo,
  } from "assets/image";
  
  export const data = [
    {
      logo: JavascriptLogo,
      id: "jslogo-1",
    },
    {
      logo: JavascriptLogo,
      id: "jslogo-2",
    },
    {
      logo: NextJSLogo,
      id: "nextjs-1",
    },
    {
      logo: NextJSLogo,
      id: "nextjs-2",
    },
    {
      logo: NodeJSLogo,
      id: "nodejs-1",
    },
    {
      logo: NodeJSLogo,
      id: "nodejs-2",
    },
    {
      logo: NpmLogo,
      id: "npmlogo-1",
    },
    {
      logo: NpmLogo,
      id: "npmlogo-2",
    },
    {
      logo: ReactNativeFirebaseLogo,
      id: "rnflogo-1",
    },
    {
      logo: ReactNativeFirebaseLogo,
      id: "rnflogo-2",
    },
    {
      logo: ReactLogo,
      id: "reactlogo-1",
    },
    {
      logo: ReactLogo,
      id: "reactlogo-2",
    },
    {
      logo: ReduxLogo,
      id: "reduxlogo-1",
    },
    {
      logo: ReduxLogo,
      id: "reduxlogo-2",
    },
    {
      logo: TypescriptLogo,
      id: "typescriptlogo-1",
    },
    {
      logo: TypescriptLogo,
      id: "typescriptlogo-2",
    },
  ];