import * as React from "react";
import { Box, styled } from "@mui/material";
import LinearProgress, {linearProgressClasses} from "@mui/material/LinearProgress";

interface Props {
  progress: number;
}

const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
  height: 15,
  borderRadius: 10,
  [`&.${linearProgressClasses.colorPrimary}`]: {
    backgroundColor:
      theme.palette.grey[theme.palette.mode === "light" ? 200 : 800],
  },
  [`& .${linearProgressClasses.bar}`]: {
    borderRadius: 10,
    backgroundColor: theme.palette.mode === "light" ? "#1a90ff" : "#308fe8",
  },
}));

const Timeline = ({ progress }: Props) => {
  return (
    <Box sx={{ width: "40%" }}>
      <BorderLinearProgress variant="determinate" value={progress} />
    </Box>
  );
};

export default Timeline;
