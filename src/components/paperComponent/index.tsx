import classnames from "classnames";
import Paper from "@mui/material/Paper";
import ReactCardFlip from "react-card-flip";

import "styles/paperComponent.sass";

interface Props {
  flippedTemp?: boolean;
  flippedFailed?: boolean;
  flippedSucc?: boolean;
  image: string;
  onClick(): void;
}

const PaperComponent = ({
  flippedTemp = false,
  flippedSucc = false,
  flippedFailed = false,
  onClick,
  image,
}: Props) => {
  const flipped = flippedTemp || flippedSucc;
  const onCardClick = () => {
    if (flipped) {
      return;
    }
    onClick();
  };

  return (
    <ReactCardFlip isFlipped={flipped}>
      <Paper
        data-testid="paper-withoutImg"
        className={classnames("paper", {
          flippedTemp,
          flippedSucc,
          flippedFailed,
          disabled: flipped,
        })}
        elevation={flipped ? 12 : 0}
        onClick={onCardClick}
      />
      <Paper
        data-testid="paper-withImg"
        className={classnames("paper", {
          flippedTemp,
          flippedSucc,
          flippedFailed,
          disabled: flipped,
        })}
        elevation={flipped ? 12 : 0}
        onClick={onCardClick}
      >
        {flipped && <img src={image} alt="img" />}
      </Paper>
    </ReactCardFlip>
  );
};

export default PaperComponent;
