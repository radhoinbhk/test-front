
import { Typography, styled } from "@mui/material";

export const Style = {
  Typography: styled(Typography)`
    margin-bottom: 50px;
  `,
};
