import * as React from "react";
import { Dialog, Button } from "@mui/material";
import { Style } from "./Dialog.style";
import "styles/dialog.sass";

interface Props {
  resultOfGame: string;
  gameOver: boolean;
  setNewGame: () => any;
}

const DialogComponent = ({ gameOver, setNewGame, resultOfGame }: Props) => {
  return (
    <Dialog fullWidth={true} maxWidth="sm" open={gameOver}>
      <div className="container">
        <Style.Typography variant="h6" gutterBottom>
          GAME OVER {resultOfGame}
        </Style.Typography>
        <Button
          onClick={() => setNewGame()}
          className="button"
          variant="outlined"
        >
          new game
        </Button>
      </div>
    </Dialog>
  );
};

export default DialogComponent;
